Vue = require 'vue'

Vue.component 'menu', require './menu'

new Vue
    el: '#main'
    data: () ->
        titles: [ 'ファイル', '編集', '表示' ]
        members: [
            { name: '松田 美優',   mail: 'matsuda_myuu@example.com',      gender: '女性', birthday: '1967-02-01' }
            { name: '筧 はるか',   mail: 'kakei_haruka@example.com',      gender: '女性', birthday: '1945-10-10' }
            { name: '武藤 慎之介', mail: 'mutou_shinnosuke@example.com',  gender: '男性', birthday: '1961-12-09' }
            { name: '宮本 直人',   mail: 'miyamoto_naoto@example.com',    gender: '男性', birthday: '1984-12-04' }
            { name: '水谷 克実',   mail: 'mizutani_katsumi@example.com',  gender: '男性', birthday: '1939-02-17' }
            { name: '楠 智花',     mail: 'kusunoki_tomoka@example.com',   gender: '女性', birthday: '1936-01-13' }
            { name: '三島 薫',     mail: 'mishima_kaoru@example.com',     gender: '男性', birthday: '1990-12-14' }
            { name: '猪股 華子',   mail: 'inomata_hanako@example.com',    gender: '女性', birthday: '1979-09-02' }
            { name: '江口 勇',     mail: 'eguchi_yuu@example.com',        gender: '女性', birthday: '1948-06-25' }
            { name: '畠中 良介',   mail: 'hatanaka_ryousuke@example.com', gender: '男性', birthday: '1992-09-24' }
        ]
