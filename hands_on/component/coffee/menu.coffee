Vue = require 'vue'

module.exports = Vue.extend
    # コンポーネントが受け取る属性
    props: [ 'titleList', 'search' ]

    # 使用するデータ
    data: () ->
        id: 1 # ここでは使ってない

    # メニューに使用するhtml
    template: """
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li v-for="title in titleList">
                            <a href="#">{{ title }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    """
