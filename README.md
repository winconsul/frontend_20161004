frontend_1004
===============

ディレクトリ構成について

* presentation
  今回のプレゼンです。
  ```
  cd presentation && npm install && npm run start
  ```
* demo
  vue.jsを使ったアプリケーションの例です。
* hands_on
  * datepicker
    npmで公開されている[vue-datepicker](https://github.com/hilongjw/vue-datepicker)を使用した例です。
