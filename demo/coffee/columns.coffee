Vue = require 'vue'

module.exports = Vue.extend
    props: ['columns', 'tables', 'options']
    methods:
        table_label_of: (table) ->
            t.label for t in @options.tables when t.name is table.table_name

        columns_of: (column) ->
            table = t for t in @tables when t.id is column.table_id

            return [] if not table?

            columns = t.columns for t in @options.tables when t.name is table.table_name
            return columns

        add_column: () ->
            return if @tables.length is 0
           
            @columns.push
                id: Math.max(c.id for c in @columns) or 0
                table_id: -1
                column_name: null
            return
        delete_column: (index) ->
            @columns.splice index

    template: """
        <section class="container">
            <div class="row form-group">
                <div class="col-sm-3 form-control-static">
                    カラム
                </div>
            </div>
            <div class="row form-group" v-for="column in columns">
                <div class="col-sm-3 col-sm-offset-1">
                    <select class="form-control" v-model="column.table_id">
                        <option v-for="table in tables" :value="table.id">
                            {{ table_label_of(table) }} ({{ table.alias }})
                        </option>
                    </select>
                </div>
                <div class="col-sm-7">
                    <select class="form-control" v-model="column.column_name">
                        <option v-for="c in columns_of(column)" :value="c.name">
                            {{ c.label }}
                        </option>
                    </select>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-danger" @click="delete_column($index)">削除</button>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-1 col-sm-offset-11">
                    <button type="button" class="btn btn-primary" @click="add_column">追加</button>
                </div>
            </div>
        </section>
    """
