Vue = require 'vue'

Vue.component 'sqlColumns', require './columns'
Vue.component 'sqlTables', require './tables'
main = new Vue
    el: "#main"
    data: () ->
        tables: [
            { id: 0, table_name: 'users', alias: 'u1'}
        ]
        columns: [
            { id: 0, table_id: 0, column_name: 'name' }
        ]
        options:
            tables: [{
                name: 'users'
                label: 'ユーザ'
                columns: [
                    { name: 'code',          label: 'ユーザコード' }
                    { name: 'name',          label: '名前' }
                    { name: 'age',           label: '年齢' }
                    { name: 'company_code',  label: '企業コード' }
                    { name: 'division_code', label: '部署コード' }
                ]
            }, {
                name: 'companies'
                label: '企業'
                columns: [
                    { name: 'code',    label: '企業コード' }
                    { name: 'name',    label: '名前' }
                    { name: 'address', label: '住所' }
                ]
            }, {
                name: 'divisions'
                label: '部署'
                columns: [
                    { name: 'code',         label: '部署コード' }
                    { name: 'company_code', label: '企業コード' }
                    { name: 'name',         label: '名前' }
                ]
            }]
