Vue = require 'vue'

module.exports = Vue.extend
    props: ['columns', 'tables', 'options']
    methods:
        add_table: () ->
            max = Math.max(table.id for table in @tables) or 0

            @tables.push
                id: max + 1
                table_name: null
                alias: ''
            return

        delete_table: (index) ->
            table = @tables[index]

            for i in [@columns.length - 1 .. 0]
                @columns.splice i if @columns[i].table_id is table.id

            @tables.splice index
            return

    template: """
        <section class="container">
            <div class="row form-group">
                <div class="col-sm-3 form-control-static">
                    テーブル
                </div>
            </div>
            <div class="row form-group" v-for="table in tables">
                <div class="col-sm-3 col-sm-offset-1">
                    <select class="form-control" v-model="table.table_name">
                        <option v-for="t in options.tables" :value="t.name">
                            {{ t.label }}
                        </option>
                    </select>
                </div>
                <div class="col-sm-7">
                    <input type="text" class="form-control" v-model="table.alias" placeholder="別名">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-danger" @click="delete_table($index)">削除</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1 col-sm-offset-11">
                    <button type="button" class="btn btn-primary" @click="add_table">追加</button>
                </div>
            </div>
        </section>
    """
