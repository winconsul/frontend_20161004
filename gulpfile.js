'use strict';

var browserify  = require('browserify');
var gulp        = require('gulp');
var filter      = require('gulp-filter');
var notify      = require('gulp-notify');
var rename      = require('gulp-rename');
var watch       = require('gulp-watch');
var source      = require('vinyl-source-stream');
var path        = require('path');

function prepare(name, options) {
    var BUILD_TASK = 'build-' + name;
    var WATCH_TASK = 'watch-' + name;

    gulp.task(BUILD_TASK, function() {
        return browserify({
            entries: [ options.src_dir + '/main.coffee' ],
            extensions: ['.coffee'],
            transform: ['coffeeify']
        }).bundle()
          .on('error', function(){
              notify.onError({
                  title: 'compile error',
                  message: '<%= error %>'
              }).apply(this, Array.prototype.slice.call(arguments));
              this.emit('end');
          })
          .pipe(source('main.js'))
          .pipe(rename('bundle.js'))
          .pipe(gulp.dest(options.dst_dir));
    });

    gulp.task(WATCH_TASK, function(){
        gulp.start(BUILD_TASK);

        watch(options.src_dir + '/**/*', function(){
            gulp.start(BUILD_TASK);
        });
    });
}

prepare('demo', {
    src_dir: 'demo/coffee',
    dst_dir: 'demo'
});
prepare('hands_on-datepicker', {
    src_dir: 'hands_on/datepicker/coffee',
    dst_dir: 'hands_on/datepicker'
});
prepare('hands_on-component', {
    src_dir: 'hands_on/component/coffee',
    dst_dir: 'hands_on/component'
});

gulp.task('watch', ['watch-demo', 'watch-hands_on-datepicker', 'watch-hands_on-component']);

gulp.task('default', ['watch']);
